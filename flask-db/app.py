from flask import Flask
from flask import request
import os

app = Flask(__name__)

file_name = os.environ['DB_FILE']


def save_to_file(text, file):
    f = open(file, "a+")
    f.write(text)
    f.close


def read_from_file(file):
    f = open(file, "r")
    text = f.read()
    f.close()
    return text


@app.route('/', methods = ['POST', 'GET'])
def hello_world():
    if request.method == "GET":
        text = read_from_file(file_name)
        return text
    else:
        text = request.stream.read().decode("utf-8")
        print(text)
        save_to_file(text, file_name)
        return "OK"

if __name__ == '__main__':
      app.run(host='0.0.0.0', port=8080)