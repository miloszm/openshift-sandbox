from flask import Flask
import requests
import os
app = Flask(__name__)

url = os.environ['FLASK_DB_HOST']

@app.route('/')
def hello_world():
    text = requests.get(url).content.decode("utf-8")
    return 'Received ' + text

if __name__ == '__main__':
      app.run(host='0.0.0.0', port=8080)